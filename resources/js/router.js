import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
const routes =  [
  {
    path: '/',
    component: () => import('./components/Login'),
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('./components/Menu'),
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ './components/pages/Settings')
  },
  {
    path: '/books',
    name: 'Books Management',
    component: () => import(/* webpackChunkName: "books" */ './components/pages/Books')
  },
  {
    path: '/patron',
    name: 'Patron Management',
    component: () => import(/* webpackChunkName: "patron" */ './components/pages/Patron')
  },
]

export default new Router({
  mode: 'history',
  routes
})