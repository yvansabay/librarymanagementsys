import moment from 'moment'
import store from './store'
import router from './router.js'

require('./bootstrap');
window.Vue = require('vue').default;

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Toast from "vue-toastification";

Vue.use(BootstrapVue)
Vue.component('main-component', () => import('./App.vue'));
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";

Vue.filter('formatDate', function (data) {
    if (data) {
        return moment(String(data)).format('MMMM Do YYYY, h:mm a');
    }
})

Vue.use(Toast, {
    transition: "Vue-Toastification__bounce",
    maxToasts: 4,
  });

const app = new Vue({
    el: '#app',
    store,
    router
});
