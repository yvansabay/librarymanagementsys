import API from "../../base/config";

const BORROWED = 'borrowedbook'

export const createBorrowed = async({commit}, {data, index}) => {
  const res = await API.post(`${BORROWED}`, data)
  .then(response => {
    commit('books/UPDATE_COPIES', {id: index, data: response.data.borrowedbook.book.copies}, {root: true})
    return response
  })
  .catch(error => {
    return error.response
  })

  return res;
}

