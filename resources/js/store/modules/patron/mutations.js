import Vue from 'vue'

export const SET_PATRONS = (state, patrons) => {
  state.patrons = patrons;
}

export const DELETE_PATRON = (state, id) => {
  state.patrons = state.patrons.filter(patron => {
    return patron.id !== id;
  });
}

export const UPDATE_PATRON = (state, {index, data}) => {
  Vue.set(state.patrons, index, data);
}

export const SAVE_PATRON = (state, patron) => {
  state.patrons.push({ 
    id: patron.id,
    first_name: patron.first_name,
    middle_name: patron.middle_name,
    last_name: patron.last_name,
    email: patron.email,
    created_at: patron.created_at
  });
}

