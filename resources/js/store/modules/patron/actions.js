import API from "../../base/config";

const PATRONS = 'patrons';

export const getPatrons = ({ commit }) => {
  API.get(PATRONS).then(response => {
    commit('SET_PATRONS', response.data);
  });
}

export const updatePatron = async ({ commit }, { index, data }) => {
  const res = await API.put(`${PATRONS}/${data.id}`, data)
    .then(response => {
      commit('UPDATE_PATRON', { index, data })
      return response;
    })
    .catch(error => {
      return error.response;
    });

  return res;
}

export const storePatron = async ({ commit }, patron) => {
  const res = await API.post(PATRONS, patron).then(response => {
    commit('SAVE_PATRON', response.data);
    return response;
  }).catch(error => {
    return error.response
  });

  return res;
}

export const deletePatron = async ({ commit }, id) => {
  const res = await API.delete(`${PATRONS}/${id}`)
    .then(response => {
      commit('DELETE_PATRON', id)
      return response;
    })


  return res;
}