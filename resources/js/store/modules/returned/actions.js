import API from "../../base/config";

const RETURNED = 'returnedbook'

export const createReturned = async({commit}, {data, index}) => {
  const res = await API.post(`${RETURNED}`, data)
  .then(response => {
    commit('books/UPDATE_COPIES', {id: index, data: response.data.book.book.copies}, {root: true})
    return response
  })
  .catch(error => {
    return error.response
  })

  return res;
}

