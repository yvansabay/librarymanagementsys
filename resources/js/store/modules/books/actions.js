import API from "../../base/config";

const BOOKS = 'books';
const CATEGORY = 'categories'

export const getCategories = async({commit}) => {
  const res = await API.get(CATEGORY).then(response => {
    commit('SET_CATEGORIES', response.data)
    return response
  }).catch(error => {
    return error.response
  });

  return res;
}

export const getBooks = async({ commit }) => {
  const res = await API.get(BOOKS).then(response => {
    commit('SET_BOOKS', response.data)
    return response
  }).catch(error => {
    return error.response;
  })
  return res
}

export const storeBook = async({ commit }, book) => {
  const res = await API.post(`${BOOKS}`, book).then(response => {
    commit('SAVE_BOOK', response.data);
    return response
  }).catch(error => {
    return error.response
  });
  
  return res;
}

export const updateBook = async({ commit },  {index, data}) => {
  const res = await API.put(`${BOOKS}/${data.id}`, data).then(response => {
    commit('UPDATE_BOOK', {id: index, data: response.data.book});
    return response;
  }).catch(error => {
    return error.response
  });

  return res;
}

export const deleteBook = async({ commit }, id) => {
  const res = await API.delete(`${BOOKS}/${id}`).then(response => {
    commit('DELETE_BOOK', id);
    return response;
  });
  return res;
}