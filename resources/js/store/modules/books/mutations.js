import Vue from 'vue';

export const SET_CATEGORIES = (state, categories) => {
  state.categories = categories
}

export const SET_BOOKS = (state, books) => {
  state.books = books;
}

export const DELETE_BOOK = (state, id) => {
  state.books = state.books.filter(book => {
    return book.id !== id;
  });
}

export const UPDATE_BOOK = (state, {id, data}) => {
  Vue.set(state.books, id, data);
}

export const SAVE_BOOK = (state, book) => {
  state.books.push(book);
}

export const UPDATE_COPIES = (state, {id, data}) => {
  state.books[id].copies = data
}