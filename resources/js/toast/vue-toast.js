const config = {
  position: "top-right",
  timeout: 4500,
  closeOnClick: true,
  pauseOnFocusLoss: false,
  pauseOnHover: false,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: "button",
  icon: true,
  rtl: false,
}

export const toast = {
  methods: {
    showDefaultToast(msg) {
      this.$toast(msg, config);
    },
    showSuccessToast(msg) {
      this.$toast.success(msg, config);
    },
    showErrorToast(msg) {
      this.$toast.error(msg, config);
    },
    
  }
}
