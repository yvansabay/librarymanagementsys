(self["webpackChunk"] = self["webpackChunk"] || []).push([["books"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Books.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Books.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _toast_vue_toast__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../toast/vue-toast */ "./resources/js/toast/vue-toast.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mixins: [_toast_vue_toast__WEBPACK_IMPORTED_MODULE_1__.toast],
  filters: {
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(date).format('MMMM DD, YYYY');
    }
  },
  data: function data() {
    return {
      id: '',
      data: {
        name: '',
        author: '',
        copies: '',
        category_id: ''
      },
      modalId: '',
      book_data: {
        book_id: '',
        copies: '',
        patron_id: ''
      },
      validated: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    document.title = "Book Management";
    this.getBooks();
    this.getCategories();
    this.getPatrons();
    this.$root.$on('bv::modal::show', function (bvEvent, modalId) {
      _this.modalId = modalId;
    });
    this.$root.$on('bv::modal::hide', function () {
      _this.clearData();
    });
  },
  computed: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapState)("books", ["books", "categories"])), (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapState)("patrons", ["patrons"])),
  methods: _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapActions)("books", ["getBooks", "deleteBook", "storeBook", "updateBook", "getCategories"])), (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapActions)("patrons", ["getPatrons"])), (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapActions)("borrowed", ["createBorrowed"])), (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapActions)("returned", ["createReturned"])), (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapGetters)("books", ["getBooksData"])), {}, {
    borrowBook: function borrowBook() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2.book_data.patron_id == '' ? delete _this2.book_data.patron_id : '';
                _context.next = 3;
                return _this2.createBorrowed({
                  data: _this2.book_data,
                  index: _this2.id
                });

              case 3:
                res = _context.sent;
                res.status == 200 ? _this2.showSuccess('Book borrowed successfully') : _this2.showError(res.data);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    returnBook: function returnBook() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this3.book_data.patron_id == '' ? delete _this3.book_data.patron_id : '';
                _context2.next = 3;
                return _this3.createReturned({
                  data: _this3.book_data,
                  index: _this3.id
                });

              case 3:
                res = _context2.sent;

                if (res.status == 200) {
                  _this3.showSuccess('Book borrowed successfully');
                } else if (res.status == 404) {
                  _this3.showErrorToast('Book to be returned doesnt match any from borrowed books');
                } else {
                  _this3.showError(res.data);
                }

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    saveBook: function saveBook() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this4.validateFields(_this4.data);

                if (_this4.validated) {
                  _context3.next = 3;
                  break;
                }

                return _context3.abrupt("return");

              case 3:
                _context3.next = 5;
                return _this4.storeBook(_objectSpread({}, _this4.data));

              case 5:
                res = _context3.sent;
                res.status == 200 ? _this4.showSuccess('Book saved successfully!') : _this4.showError(res.data);

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    bookDelete: function bookDelete() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee4() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return _this5.deleteBook(_this5.id);

              case 2:
                res = _context4.sent;
                if (res.status !== 200) _this5.showError(res.data);

                _this5.$bvModal.hide('deleteModal');

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    bookUpdate: function bookUpdate() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee5() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _this6.validateFields(_this6.data);

                if (_this6.validated) {
                  _context5.next = 3;
                  break;
                }

                return _context5.abrupt("return");

              case 3:
                _context5.next = 5;
                return _this6.updateBook({
                  index: _this6.id,
                  data: _this6.data
                });

              case 5:
                res = _context5.sent;
                res.status == 200 ? _this6.showSuccess('Patron updated successfully!') : _this6.showError(res.data);

              case 7:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    showSuccess: function showSuccess(msg) {
      this.$bvModal.hide(this.modalId);
      this.showSuccessToast(msg);
    },
    clearData: function clearData() {
      this.data = {
        name: '',
        author: '',
        copies: '',
        category_id: ''
      };
    },
    showError: function showError(data) {
      for (var _i = 0, _Object$keys = Object.keys(data); _i < _Object$keys.length; _i++) {
        var key = _Object$keys[_i];
        this.showErrorToast(data[key][0]);
      }
    },
    validateFields: function validateFields(books) {
      var errors = [];
      this.validated = false;
      if (books.name == '') errors.push('Name of book is required!');
      if (books.author == '') errors.push('Author is required!');
      if (books.copies == '') errors.push('Copies of book is required');
      if (books.copies < 0) errors.push('Copies of book should not be negative');
      if (books.category_id == '') errors.push('Category is required!');

      if (errors.length > 0) {
        for (var e in errors) {
          this.showErrorToast(errors[e]);
        }

        return;
      }

      this.validated = true;
    }
  }),
  watch: {}
});

/***/ }),

/***/ "./resources/js/toast/vue-toast.js":
/*!*****************************************!*\
  !*** ./resources/js/toast/vue-toast.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "toast": () => (/* binding */ toast)
/* harmony export */ });
var config = {
  position: "top-right",
  timeout: 4500,
  closeOnClick: true,
  pauseOnFocusLoss: false,
  pauseOnHover: false,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: "button",
  icon: true,
  rtl: false
};
var toast = {
  methods: {
    showDefaultToast: function showDefaultToast(msg) {
      this.$toast(msg, config);
    },
    showSuccessToast: function showSuccessToast(msg) {
      this.$toast.success(msg, config);
    },
    showErrorToast: function showErrorToast(msg) {
      this.$toast.error(msg, config);
    }
  }
};

/***/ }),

/***/ "./resources/js/components/pages/Books.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/pages/Books.vue ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Books_vue_vue_type_template_id_8f781ff8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Books.vue?vue&type=template&id=8f781ff8& */ "./resources/js/components/pages/Books.vue?vue&type=template&id=8f781ff8&");
/* harmony import */ var _Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Books.vue?vue&type=script&lang=js& */ "./resources/js/components/pages/Books.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Books_vue_vue_type_template_id_8f781ff8___WEBPACK_IMPORTED_MODULE_0__.render,
  _Books_vue_vue_type_template_id_8f781ff8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pages/Books.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/pages/Books.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/pages/Books.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Books.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Books.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/pages/Books.vue?vue&type=template&id=8f781ff8&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/pages/Books.vue?vue&type=template&id=8f781ff8& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_template_id_8f781ff8___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_template_id_8f781ff8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Books_vue_vue_type_template_id_8f781ff8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Books.vue?vue&type=template&id=8f781ff8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Books.vue?vue&type=template&id=8f781ff8&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Books.vue?vue&type=template&id=8f781ff8&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Books.vue?vue&type=template&id=8f781ff8& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "app" } },
    [
      _c("div", { staticClass: "container pt-4 mb-3" }, [
        _c("h6", { staticClass: "text-muted" }, [
          _vm._v("Here are your books")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mt-2" }, [
          _c("div", { staticClass: "col-11 col-sm-11 col-md-11 col-lg-11" }, [
            _c(
              "div",
              { staticClass: "row justify-content-end mr-1 mb-3" },
              [
                _c(
                  "b-button",
                  {
                    attrs: {
                      variant: "btn rounded-pill btn-outline-primary btn-sm"
                    },
                    on: {
                      click: function($event) {
                        return _vm.$bvModal.show("addModal")
                      }
                    }
                  },
                  [_vm._v("\n              Add Books\n            ")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "card p-4 card-shadow" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table table-hover" }, [
                  _vm._m(0),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.books, function(book, i) {
                      return _c("tr", { key: i }, [
                        _c("td", [_vm._v(_vm._s(book.name))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(book.author))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(book.copies))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(book.category.category))]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(_vm._s(_vm._f("moment")(book.created_at)))
                        ]),
                        _vm._v(" "),
                        _c("td", [
                          _c(
                            "a",
                            {
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  _vm.$bvModal.show("editModal")
                                  _vm.id = i
                                  _vm.data = Object.assign({}, book)
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "mr-2",
                                attrs: { src: "images/edit.png", height: "25" }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  $event.preventDefault()
                                  _vm.$bvModal.show("deleteModal")
                                  _vm.id = book.id
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "mr-2",
                                attrs: {
                                  src: "images/delete.png",
                                  height: "16"
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  _vm.book_data.book_id = book.id
                                  _vm.id = i
                                  _vm.$bvModal.show("borrowModal")
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "cursor-pointer mr-2",
                                attrs: {
                                  src: "images/borrow.png",
                                  height: "20"
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  _vm.book_data.book_id = book.id
                                  _vm.id = i
                                  _vm.$bvModal.show("returnModal")
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "cursor-pointer",
                                attrs: {
                                  src: "images/return.png",
                                  height: "20"
                                }
                              })
                            ]
                          )
                        ])
                      ])
                    }),
                    0
                  )
                ])
              ]),
              _vm._v(" "),
              _vm._m(1)
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "addModal", centered: "", title: "Add Book" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function() {
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { size: "sm", variant: "primary" },
                      on: { click: _vm.saveBook }
                    },
                    [_vm._v("\n        Add Book\n      ")]
                  )
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _c("form", { staticClass: "pl-5 pr-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "name" } }, [_vm._v("Name")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.data.name,
                    expression: "data.name"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "name", placeholder: "" },
                domProps: { value: _vm.data.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.data, "name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "author" } }, [_vm._v("Author")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.data.author,
                    expression: "data.author"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "author", placeholder: "" },
                domProps: { value: _vm.data.author },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.data, "author", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "copies" } }, [_vm._v("Copies")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.data.copies,
                    expression: "data.copies"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "copies", placeholder: "", type: "number" },
                domProps: { value: _vm.data.copies },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.data, "copies", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "customSelect" } }, [
                _vm._v("Category")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.data.category_id,
                      expression: "data.category_id"
                    }
                  ],
                  staticClass: "custom-select",
                  attrs: { id: "customSelect", name: "cars" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.data,
                        "category_id",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option", { attrs: { selected: "", disabled: "" } }, [
                    _vm._v("Please select a category")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.categories, function(categ, i) {
                    return _c(
                      "option",
                      { key: i, domProps: { value: categ.id } },
                      [_vm._v(_vm._s(categ.category))]
                    )
                  })
                ],
                2
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "editModal", centered: "", title: "Edit Book" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function(ref) {
                var cancel = ref.cancel
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "primary", size: "sm" },
                      on: {
                        click: function($event) {
                          return cancel()
                        }
                      }
                    },
                    [_vm._v(" Cancel ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-button",
                    {
                      attrs: { size: "sm", variant: "success" },
                      on: { click: _vm.bookUpdate }
                    },
                    [_vm._v("\n        Update Book\n      ")]
                  )
                ]
              }
            }
          ])
        },
        [
          _c("form", { staticClass: "pl-5 pr-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "name" } }, [_vm._v("Name")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.data.name,
                    expression: "data.name"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "name", placeholder: "" },
                domProps: { value: _vm.data.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.data, "name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "author" } }, [_vm._v("Author")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.data.author,
                    expression: "data.author"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "author", placeholder: "" },
                domProps: { value: _vm.data.author },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.data, "author", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "copies" } }, [_vm._v("Copies")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.data.copies,
                    expression: "data.copies"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "copies", placeholder: "", type: "number" },
                domProps: { value: _vm.data.copies },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.data, "copies", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "customSelect" } }, [
                _vm._v("Category")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.data.category_id,
                      expression: "data.category_id"
                    }
                  ],
                  staticClass: "custom-select",
                  attrs: { id: "customSelect", name: "cars" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.data,
                        "category_id",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option", { attrs: { selected: "", disabled: "" } }, [
                    _vm._v("Please select a category")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.categories, function(categ, i) {
                    return _c(
                      "option",
                      { key: i, domProps: { value: categ.id } },
                      [_vm._v(_vm._s(categ.category))]
                    )
                  })
                ],
                2
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "deleteModal", centered: "", title: "Confirm Delete" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function(ref) {
                var cancel = ref.cancel
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "info" },
                      on: {
                        click: function($event) {
                          return cancel()
                        }
                      }
                    },
                    [_vm._v(" Cancel ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "danger" },
                      on: { click: _vm.bookDelete }
                    },
                    [_vm._v(" Confirm ")]
                  )
                ]
              }
            }
          ])
        },
        [
          _c("p", { staticClass: "my-4" }, [
            _vm._v("Are you sure you want to delete this book?")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "borrowModal", centered: "", title: "Borrow Book" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function(ref) {
                var cancel = ref.cancel
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "primary", size: "sm" },
                      on: {
                        click: function($event) {
                          return cancel()
                        }
                      }
                    },
                    [_vm._v(" Cancel ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-button",
                    {
                      attrs: { size: "sm", variant: "success" },
                      on: { click: _vm.borrowBook }
                    },
                    [_vm._v("\n        Borrow Book\n      ")]
                  )
                ]
              }
            }
          ])
        },
        [
          _c("form", { staticClass: "pl-5 pr-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "copies" } }, [_vm._v("Copies")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.book_data.copies,
                    expression: "book_data.copies"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "copies", placeholder: "", type: "number" },
                domProps: { value: _vm.book_data.copies },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.book_data, "copies", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "customSelect" } }, [
                _vm._v("Patron")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.book_data.patron_id,
                      expression: "book_data.patron_id"
                    }
                  ],
                  staticClass: "custom-select",
                  attrs: { id: "customSelect", name: "patrons" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.book_data,
                        "patron_id",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option", { attrs: { selected: "", disabled: "" } }, [
                    _vm._v("Please select a patron (optional)")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.patrons, function(patron, i) {
                    return _c(
                      "option",
                      { key: i, domProps: { value: patron.id } },
                      [
                        _vm._v(
                          "  " +
                            _vm._s(
                              patron.first_name +
                                " " +
                                patron.middle_name +
                                " " +
                                patron.last_name
                            )
                        )
                      ]
                    )
                  })
                ],
                2
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "returnModal", centered: "", title: "Return Book" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function(ref) {
                var cancel = ref.cancel
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "primary", size: "sm" },
                      on: {
                        click: function($event) {
                          return cancel()
                        }
                      }
                    },
                    [_vm._v(" Cancel ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-button",
                    {
                      attrs: { size: "sm", variant: "success" },
                      on: { click: _vm.returnBook }
                    },
                    [_vm._v("\n        Return Book\n      ")]
                  )
                ]
              }
            }
          ])
        },
        [
          _c("form", { staticClass: "pl-5 pr-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "copies" } }, [_vm._v("Copies")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.book_data.copies,
                    expression: "book_data.copies"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "copies", placeholder: "", type: "number" },
                domProps: { value: _vm.book_data.copies },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.book_data, "copies", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "customSelect" } }, [
                _vm._v("Patron")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.book_data.patron_id,
                      expression: "book_data.patron_id"
                    }
                  ],
                  staticClass: "custom-select",
                  attrs: { id: "customSelect", name: "patrons" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.book_data,
                        "patron_id",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option", { attrs: { selected: "", disabled: "" } }, [
                    _vm._v("Please select a patron (optional)")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.patrons, function(patron, i) {
                    return _c(
                      "option",
                      { key: i, domProps: { value: patron.id } },
                      [
                        _vm._v(
                          "  " +
                            _vm._s(
                              patron.first_name +
                                " " +
                                patron.middle_name +
                                " " +
                                patron.last_name
                            )
                        )
                      ]
                    )
                  })
                ],
                2
              )
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Name")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Author")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [
          _vm._v("Copies "),
          _c("br"),
          _vm._v("Available")
        ]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Category")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Created At")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Actions")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "nav",
      {
        staticClass: "mt-3",
        attrs: { "aria-label": "Page navigation example" }
      },
      [
        _c(
          "ul",
          { staticClass: "pagination pagination-sm justify-content-end" },
          [
            _c("li", { staticClass: "page-item disabled" }, [
              _c(
                "a",
                {
                  staticClass: "page-link",
                  attrs: { href: "#", tabindex: "-1" }
                },
                [_vm._v("Prev")]
              )
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item active" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("1")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("Next")
              ])
            ])
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);