(self["webpackChunk"] = self["webpackChunk"] || []).push([["patron"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Patron.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Patron.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _toast_vue_toast__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../toast/vue-toast */ "./resources/js/toast/vue-toast.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mixins: [_toast_vue_toast__WEBPACK_IMPORTED_MODULE_1__.toast],
  filters: {
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(date).format('MMMM DD, YYYY');
    }
  },
  mounted: function mounted() {
    var _this = this;

    document.title = "Patron Management";
    this.$root.$on('bv::modal::hide', function () {
      _this.clearFields();
    });
    this.$root.$on('bv::modal::show', function (bvEvent, modalId) {
      _this.modalId = modalId;
    });
    this.getPatrons();
    this.getPatronData();
  },
  computed: _objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapState)('patrons', ['patrons'])),
  data: function data() {
    return {
      input: {
        first_name: "",
        middle_name: "",
        last_name: "",
        email: ""
      },
      modalId: '',
      index: "",
      validated: false
    };
  },
  methods: _objectSpread(_objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapActions)('patrons', ['getPatrons', 'storePatron', 'deletePatron', 'updatePatron'])), (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapGetters)('patrons', ['getPatronData'])), {}, {
    validEmail: function validEmail() {
      var regEx = /^([a-z0-9_\-.])+@([a-z0-9_\-.])+\.([a-z0-9]{2,})$/gi;
      var isEmail = regEx.test(this.input.email);
      return isEmail;
    },
    savePatron: function savePatron() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2.validateFields(_this2.input);

                if (_this2.validated) {
                  _context.next = 3;
                  break;
                }

                return _context.abrupt("return");

              case 3:
                _context.next = 5;
                return _this2.storePatron(_this2.input);

              case 5:
                res = _context.sent;
                res.status == 200 ? _this2.showSuccess('Patron saved successfully!') : _this2.showError(res.data);

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    patronDelete: function patronDelete() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _this3.deletePatron(_this3.index);

              case 2:
                res = _context2.sent;
                if (res.status !== 200) _this3.showError(res.data);

                _this3.$bvModal.hide('deleteModal');

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    patronUpdate: function patronUpdate() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3() {
        var res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this4.validateFields(_this4.input);

                if (_this4.validated) {
                  _context3.next = 3;
                  break;
                }

                return _context3.abrupt("return");

              case 3:
                _context3.next = 5;
                return _this4.updatePatron({
                  data: _objectSpread({}, _this4.input),
                  index: _this4.index
                });

              case 5:
                res = _context3.sent;
                res.status == 200 ? _this4.showSuccess('Patron updated successfully!') : _this4.showError(res.data);

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    showSuccess: function showSuccess(msg) {
      this.$bvModal.hide(this.modalId);
      this.showSuccessToast(msg);
    },
    clearFields: function clearFields() {
      this.input.first_name = "";
      this.input.middle_name = "";
      this.input.last_name = "";
      this.input.email = "";
    },
    showError: function showError(data) {
      for (var _i = 0, _Object$keys = Object.keys(data); _i < _Object$keys.length; _i++) {
        var key = _Object$keys[_i];
        this.showErrorToast(data[key][0]);
      }
    },
    validateFields: function validateFields(input) {
      var errors = [];
      this.validated = false;
      if (input.first_name == "") errors.push("First Name is required!");
      if (input.last_name == "") errors.push("Last Name is required!");

      if (input.email == "") {
        errors.push("Email is required!");
      }

      if (!this.validEmail()) errors.push("Email is not valid");

      if (errors.length > 0) {
        for (var e in errors) {
          this.showErrorToast(errors[e]);
        }

        return;
      }

      this.validated = true;
    }
  })
});

/***/ }),

/***/ "./resources/js/toast/vue-toast.js":
/*!*****************************************!*\
  !*** ./resources/js/toast/vue-toast.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "toast": () => (/* binding */ toast)
/* harmony export */ });
var config = {
  position: "top-right",
  timeout: 4500,
  closeOnClick: true,
  pauseOnFocusLoss: false,
  pauseOnHover: false,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: "button",
  icon: true,
  rtl: false
};
var toast = {
  methods: {
    showDefaultToast: function showDefaultToast(msg) {
      this.$toast(msg, config);
    },
    showSuccessToast: function showSuccessToast(msg) {
      this.$toast.success(msg, config);
    },
    showErrorToast: function showErrorToast(msg) {
      this.$toast.error(msg, config);
    }
  }
};

/***/ }),

/***/ "./resources/js/components/pages/Patron.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/pages/Patron.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Patron_vue_vue_type_template_id_06c37664___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Patron.vue?vue&type=template&id=06c37664& */ "./resources/js/components/pages/Patron.vue?vue&type=template&id=06c37664&");
/* harmony import */ var _Patron_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Patron.vue?vue&type=script&lang=js& */ "./resources/js/components/pages/Patron.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Patron_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Patron_vue_vue_type_template_id_06c37664___WEBPACK_IMPORTED_MODULE_0__.render,
  _Patron_vue_vue_type_template_id_06c37664___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pages/Patron.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/pages/Patron.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/pages/Patron.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Patron_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Patron.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Patron.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Patron_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/pages/Patron.vue?vue&type=template&id=06c37664&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/pages/Patron.vue?vue&type=template&id=06c37664& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Patron_vue_vue_type_template_id_06c37664___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Patron_vue_vue_type_template_id_06c37664___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Patron_vue_vue_type_template_id_06c37664___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Patron.vue?vue&type=template&id=06c37664& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Patron.vue?vue&type=template&id=06c37664&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Patron.vue?vue&type=template&id=06c37664&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/pages/Patron.vue?vue&type=template&id=06c37664& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "app" } },
    [
      _c("div", { staticClass: "container pt-4" }, [
        _c("h6", { staticClass: "text-muted" }, [
          _vm._v("Here are your patrons")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mt-2" }, [
          _c("div", { staticClass: "col-11 col-sm-11 col-md-11 col-lg-11" }, [
            _c("div", { staticClass: "row justify-content-end mr-1 mb-3" }, [
              _c(
                "button",
                {
                  staticClass: "btn rounded-pill btn-outline-primary btn-sm",
                  on: {
                    click: function($event) {
                      return _vm.$bvModal.show("addModal")
                    }
                  }
                },
                [_vm._v("\n            Add Patron\n          ")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card p-4 card-shadow" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table table-hover" }, [
                  _vm._m(0),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.patrons, function(patron, i) {
                      return _c("tr", { key: i }, [
                        _c("td", [
                          _vm._v(
                            "\n                    " +
                              _vm._s(
                                patron.first_name +
                                  " " +
                                  patron.middle_name +
                                  " " +
                                  patron.last_name
                              ) +
                              "\n                  "
                          )
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(patron.email))]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(_vm._s(_vm._f("moment")(patron.created_at)))
                        ]),
                        _vm._v(" "),
                        _c("td", [
                          _c(
                            "a",
                            {
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  _vm.$bvModal.show("editModal")
                                  _vm.index = i
                                  _vm.input = Object.assign({}, patron)
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "mr-2",
                                attrs: { src: "images/edit.png", height: "25" }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  _vm.$bvModal.show("deleteModal")
                                  _vm.index = patron.id
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "mr-2",
                                attrs: {
                                  src: "images/delete.png",
                                  height: "16"
                                }
                              })
                            ]
                          )
                        ])
                      ])
                    }),
                    0
                  )
                ])
              ]),
              _vm._v(" "),
              _vm._m(1)
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "addModal", centered: "", title: "Add Patron" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function() {
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { size: "sm", variant: "primary" },
                      on: { click: _vm.savePatron }
                    },
                    [_vm._v("\n        Add Patron\n      ")]
                  )
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _c("form", { ref: "addPatronModal", staticClass: "pl-5 pr-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "AddPatronFirstName" } }, [
                _vm._v("First Name")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.first_name,
                    expression: "input.first_name"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "email",
                  id: "AddPatronFirstName",
                  placeholder: ""
                },
                domProps: { value: _vm.input.first_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "first_name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "AddPatronMiddleName" } }, [
                _vm._v("Middle Name")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.middle_name,
                    expression: "input.middle_name"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  id: "AddPatronMiddleName",
                  placeholder: ""
                },
                domProps: { value: _vm.input.middle_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "middle_name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "AddPatronLastName" } }, [
                _vm._v("Last Name")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.last_name,
                    expression: "input.last_name"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  id: "AddPatronLastName",
                  placeholder: ""
                },
                domProps: { value: _vm.input.last_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "last_name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "AddPatronEmail" } }, [
                _vm._v("Email address")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.email,
                    expression: "input.email"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "AddPatronEmail", placeholder: "" },
                domProps: { value: _vm.input.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "email", $event.target.value)
                  }
                }
              })
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "editModal", centered: "", title: "Edit Patron" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function() {
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { size: "sm", variant: "primary" },
                      on: { click: _vm.patronUpdate }
                    },
                    [_vm._v("\n        Update Patron\n      ")]
                  )
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _c("form", { ref: "editPatronModal", staticClass: "pl-5 pr-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "EditPatronFirstName" } }, [
                _vm._v("First Name")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.first_name,
                    expression: "input.first_name"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "email",
                  id: "EditPatronFirstName",
                  placeholder: ""
                },
                domProps: { value: _vm.input.first_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "first_name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "EditPatronMiddleName" } }, [
                _vm._v("Middle Name")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.middle_name,
                    expression: "input.middle_name"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  id: "EditPatronMiddleName",
                  placeholder: ""
                },
                domProps: { value: _vm.input.middle_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "middle_name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "EditPatronLastName" } }, [
                _vm._v("Last Name")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.last_name,
                    expression: "input.last_name"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  id: "EditPatronLastName",
                  placeholder: ""
                },
                domProps: { value: _vm.input.last_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "last_name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "EditPatronEmail" } }, [
                _vm._v("Email address")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.input.email,
                    expression: "input.email"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "EditPatronEmail", placeholder: "" },
                domProps: { value: _vm.input.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.input, "email", $event.target.value)
                  }
                }
              })
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: { id: "deleteModal", centered: "", title: "Confirm Delete" },
          scopedSlots: _vm._u([
            {
              key: "modal-footer",
              fn: function(ref) {
                var cancel = ref.cancel
                return [
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "info", size: "sm" },
                      on: {
                        click: function($event) {
                          return cancel()
                        }
                      }
                    },
                    [_vm._v(" Cancel ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-button",
                    {
                      attrs: { variant: "danger", size: "sm" },
                      on: { click: _vm.patronDelete }
                    },
                    [_vm._v("\n        Confirm\n      ")]
                  )
                ]
              }
            }
          ])
        },
        [
          _c("p", { staticClass: "my-4" }, [
            _vm._v("Are you sure you want to delete this patron?")
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Name")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Email Address")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Date Registered")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Actions")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "nav",
      {
        staticClass: "mt-3",
        attrs: { "aria-label": "Page navigation example" }
      },
      [
        _c(
          "ul",
          { staticClass: "pagination pagination-sm justify-content-end" },
          [
            _c("li", { staticClass: "page-item disabled" }, [
              _c(
                "a",
                {
                  staticClass: "page-link",
                  attrs: { href: "#", tabindex: "-1" }
                },
                [_vm._v("Prev")]
              )
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item active" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("1")
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "page-item" }, [
              _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                _vm._v("Next")
              ])
            ])
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);